import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:test_apartment/Model/Apartment.dart';
import 'package:test_apartment/Model/FilterRequest.dart';

class ApiProvider {
  String url = 'https://kvmde23-14076.fornex.org/api/estate/';

  Future<List<Apartment>> getApartmentsList(int offset) async {
    List<Apartment> apartmentsList = List();

    var body = {
      'minPrice': FilterRequest().minPrice,
      'maxPrice': FilterRequest().maxPrice,
      'buldingStatus': FilterRequest().buildingStatus,
      'type': FilterRequest().type,
    };

    if (FilterRequest().rooms.length != 0) {
      body['rooms'] = FilterRequest().rooms.join(',');
    }
    print(body);

    var data = await this
        .post(url + FilterRequest().dealType + '/' + offset.toString(), body);

    data.forEach(
        (apartment) => apartmentsList.add(Apartment.fromJson(apartment)));

    return apartmentsList;
  }

  Future post(String path, Map body) async {
    http.Response response = await http.post(
      path,
      body: body,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "charset": 'UTF-8'
      },
    );

    switch (response.statusCode) {
      case 200:
        return json.decode(response.body);
      case 500:
        throw Exception('Server error');
    }

    throw Exception('unnamed exception');
  }
}
