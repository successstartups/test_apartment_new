import 'package:flutter/material.dart';
import 'package:test_apartment/Model/FilterRequest.dart';
import 'package:test_apartment/Other/Offsets.dart';

class PriceField extends StatefulWidget {
  @override
  _PriceFieldState createState() => _PriceFieldState();
}

class _PriceFieldState extends State<PriceField> {
  TextEditingController _priceStart = TextEditingController();

  TextEditingController _priceEnd = TextEditingController();
  var priceStartNode = FocusNode();

  String x;

  @override
  void dispose() {
    priceStartNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => {_displayDialog(context)},
        child: Padding(
            padding: EdgeInsets.only(
              left: SIDE_PADDING,
              right: SIDE_PADDING,
            ),
            child: Container(
              height: PICKER_HEIGHT,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4.0),
                  color: Colors.white),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                      padding: EdgeInsets.only(
                          left: SIDE_PADDING, right: SIDE_PADDING),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('Цена',
                              style: Theme.of(context).textTheme.body2),
                          Padding(
                              padding: EdgeInsets.only(left: 16.0),
                              child: Text(
                                  (_priceStart.text.isEmpty
                                          ? ''
                                          : 'от ${_priceStart.text}') +
                                      (_priceEnd.text.isEmpty
                                          ? ' '
                                          : " до ${_priceEnd.text}"),
                                  style: Theme.of(context).textTheme.body2))
                        ],
                      ))),
            )));
  }

  _displayDialog(BuildContext context) async {
    FocusScope.of(context).requestFocus(priceStartNode);
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Цена'),
            content: Container(
                height: 100,
                child: Column(
                  children: <Widget>[
                    TextField(
                        controller: _priceStart,
                        focusNode: priceStartNode,
                        keyboardType: TextInputType.number,
                        decoration:
                            InputDecoration(hintText: "от", suffixText: '₽')),
                    TextField(
                      controller: _priceEnd,
                      keyboardType: TextInputType.number,
                      decoration:
                          InputDecoration(hintText: "до", suffixText: '₽'),
                    ),
                  ],
                )),
            actions: <Widget>[
              FlatButton(
                  child: Text('Отмена'),
                  onPressed: () {
                    priceStartNode.unfocus();
                    Navigator.of(context).pop();
                  }),
              FlatButton(
                child: Text('Ок'),
                onPressed: () {
                  priceStartNode.unfocus();
                  FilterRequest().minPrice = _priceStart.text;
                  FilterRequest().maxPrice = _priceEnd.text;
                  Navigator.pop(context, _priceStart.text);
                },
              )
            ],
          );
        });
  }
}
