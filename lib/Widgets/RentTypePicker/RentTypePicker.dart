import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:test_apartment/Other/Offsets.dart';

class RentTypePicker extends StatefulWidget {
  @override
  _RentTypePickerState createState() => _RentTypePickerState();
}

class _RentTypePickerState extends State<RentTypePicker> {
  List<bool> buttonSelected = [true, false];

  @override
  Widget build(BuildContext context) {
    double widgetWidth =
        (MediaQuery.of(context).size.width - 2 * SIDE_PADDING) / 2;
    return Padding(
        padding: EdgeInsets.only(
            left: SIDE_PADDING, right: SIDE_PADDING, bottom: SIDE_PADDING),
        child: Container(
          child: Row(
            children: <Widget>[
              RentTypeButton(
                  width: widgetWidth,
                  title: 'Длительная',
                  index: 0,
                  color: buttonSelected[0] ? Colors.blueGrey : Colors.white,
                  onTap: () => buttonPressed(0)),
              RentTypeButton(
                  width: widgetWidth,
                  title: 'Посуточная',
                  index: 1,
                  color: buttonSelected[1] ? Colors.blueGrey : Colors.white,
                  onTap: () => buttonPressed(1)),
            ],
          ),
        ));
  }

  buttonPressed(int index) {
    clearList();
    print(buttonSelected);
    setState(() {
      buttonSelected[index] = !buttonSelected[index];
    });
  }

  clearList() {
    for (int i = 0; i < buttonSelected.length; i++) {
      buttonSelected[i] = false;
    }
  }
}

class RentTypeButton extends StatefulWidget {
  Function onTap;
  Color color;
  double width;
  String title;
  int index;

  RentTypeButton({this.color, this.onTap, this.width, this.title, this.index});

  @override
  _RentTypeButtonState createState() => _RentTypeButtonState();
}

class _RentTypeButtonState extends State<RentTypeButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: widget.onTap,
        child: Container(
          height: PICKER_HEIGHT,
          width: widget.width,
          decoration: BoxDecoration(
            color: widget.color,
            borderRadius: BorderRadius.horizontal(
                left: widget.index == 0 ? Radius.circular(4) : Radius.zero,
                right: widget.index == 1 ? Radius.circular(4) : Radius.zero),
          ),
          child: Center(
              child:
                  Text(widget.title, style: Theme.of(context).textTheme.body2)),
        ));
  }
}
