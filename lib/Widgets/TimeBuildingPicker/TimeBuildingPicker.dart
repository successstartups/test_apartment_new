import 'package:flutter/material.dart';
import 'package:test_apartment/Model/FilterRequest.dart';
import 'package:test_apartment/Other/Colors.dart';
import 'package:test_apartment/Other/Fonts.dart';
import 'package:test_apartment/Other/Offsets.dart';

class TimeBuildingPicker extends StatefulWidget {
  @override
  _TimeBuildingPickerState createState() => _TimeBuildingPickerState();
}

class _TimeBuildingPickerState extends State<TimeBuildingPicker> {
  List<bool> buttonsSelected = [true, false, false]; //по умолчанию выбраны Все

  @override
  Widget build(BuildContext context) {
    double widgetWidth = MediaQuery.of(context).size.width - 2 * SIDE_PADDING;
    return Padding(
        padding: EdgeInsets.only(
            left: SIDE_PADDING, right: SIDE_PADDING, bottom: SIDE_PADDING),
        child: Row(
          children: <Widget>[
            TimeBuildingButton(
              title: 'Все',
              width: widgetWidth / 3,
              index: 0,
              color: buttonsSelected[0] ? BLUE : Colors.white,
              onTap: () => buttonPressed(0),
              active:
                  buttonsSelected[0] ? PICKER_WHITE_TEXT : PICKER_BLACK_TEXT,
            ),
            TimeBuildingButton(
                title: 'Новостройка',
                width: widgetWidth / 3,
                index: 1,
                color: buttonsSelected[1] ? BLUE: Colors.white,
                onTap: () => buttonPressed(1),
                active:
                    buttonsSelected[1] ? PICKER_WHITE_TEXT : PICKER_BLACK_TEXT),
            TimeBuildingButton(
                title: 'Вторичка',
                width: widgetWidth / 3,
                index: 2,
                color: buttonsSelected[2] ? BLUE : Colors.white,
                onTap: () => buttonPressed(2),
                active:
                    buttonsSelected[2] ? PICKER_WHITE_TEXT : PICKER_BLACK_TEXT)
          ],
        ));
  }

  buttonPressed(int index) {
    clearList();

    setState(() {
      buttonsSelected[index] = !buttonsSelected[index];
    });

    switch (index) {
      case 0:
        FilterRequest().buildingStatus = 'Все';
        break;
      case 1:
        FilterRequest().buildingStatus = 'new_construction';
        break;
      case 2:
        FilterRequest().buildingStatus = 'old_construction';
        break;
    }

    print(FilterRequest().buildingStatus);
  }

  clearList() {
    for (int i = 0; i < buttonsSelected.length; i++) {
      buttonsSelected[i] = false;
    }
  }
}

class TimeBuildingButton extends StatefulWidget {
  final double width;
  String title;
  int index;
  Function onTap;
  Color color;
  TextStyle active;

  TimeBuildingButton(
      {this.width,
      this.title,
      this.index,
      this.onTap,
      this.color,
      this.active});

  @override
  _TimeBuildingButtonState createState() => _TimeBuildingButtonState();
}

class _TimeBuildingButtonState extends State<TimeBuildingButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: widget.onTap,
        child: Container(
          height: PICKER_HEIGHT,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.horizontal(
                  left: widget.index == 0 ? Radius.circular(4.0) : Radius.zero,
                  right:
                      widget.index == 2 ? Radius.circular(4.0) : Radius.zero),
              color: widget.color),
          width: widget.width,
          child: Center(child: Text(widget.title, style: widget.active)),
        ));
  }
}
