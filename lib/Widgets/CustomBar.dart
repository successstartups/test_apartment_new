import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_apartment/Blocs/RoomCountBloc/Filter.dart';
import 'package:test_apartment/Other/Colors.dart';
import 'package:test_apartment/Other/Offsets.dart';

class CustomBar extends StatefulWidget implements PreferredSizeWidget {
  @override
  _CustomBarState createState() => _CustomBarState();

  @override
  Size get preferredSize => Size(20, 40);
}

class _CustomBarState extends State<CustomBar> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        child: Padding(
            padding: EdgeInsets.only(bottom: 4.0),
            child: Container(
                padding: EdgeInsets.only(bottom: 4.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[_cancelButton(), _resetButton()],
                ))));
  }

  Widget _cancelButton() {
    return Container(
      padding: EdgeInsets.only(left: 4.0),
      child: Center(child: Icon(Icons.close, color: Colors.blueGrey)),
    );
  }

  Widget _resetButton() {
    return GestureDetector(
        onTap: () => {
              BlocProvider.of<FilterBloc>(context).dispatch(ResetFilter())
            },
        child: Container(
            padding: EdgeInsets.only(right: SIDE_PADDING),
            child: Center(
                child:
                    Text('Сбросить', style: TextStyle(color: BLUE)))));
  }
}
