import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_apartment/Blocs/RoomCountBloc/Filter.dart';

import 'package:test_apartment/Other/Offsets.dart';

class DealTypePicker extends StatefulWidget {
  @override
  _DealTypePickerState createState() => _DealTypePickerState();
}

class _DealTypePickerState extends State<DealTypePicker> {
  String dealType = 'Купить';
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => showDealTypeList(
          context: context,
          child: CupertinoActionSheet(
            actions: <Widget>[
              CupertinoActionSheetAction(
                  child: Text('Купить'),
                  onPressed: () => {
                        Navigator.pop(context, 'Купить'),
                        BlocProvider.of<FilterBloc>(context)
                            .dispatch(BuyTypeSelected())
                      }),
              CupertinoActionSheetAction(
                child: Text('Снять'),
                onPressed: () => {
                      Navigator.pop(context, 'Снять'),
                      BlocProvider.of<FilterBloc>(context)
                          .dispatch(RentTypeSelected())
                    },
              )
            ],
          )),
      child: Container(
        width: (MediaQuery.of(context).size.width / 2) -
            (SIDE_PADDING + SMALL_PADDING),
        margin: EdgeInsets.only(left: SIDE_PADDING, right: SMALL_PADDING),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          color: Colors.white,
        ),
        child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
                padding: EdgeInsets.only(left: 0),
                child: Padding(
                    padding: EdgeInsets.only(left: MEDIUM_PADDING),
                    child: Text(
                      dealType.toString(),
                      style: Theme.of(context).textTheme.body2,
                    )))),
      ),
    );
  }

  void showDealTypeList({BuildContext context, Widget child}) {
    showCupertinoModalPopup<String>(
      context: context,
      builder: (BuildContext context) => child,
    ).then((String value) {
      if (value != null) {
        setState(() {
          dealType = value;
        });
      }
    });
  }
}
