import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:test_apartment/Model/Apartment.dart';

import 'package:test_apartment/Other/Offsets.dart';
import 'package:test_apartment/Views/ApartmentPage.dart';

import 'package:shimmer/shimmer.dart';

class ApartmentCard extends StatelessWidget {
  Apartment apartment;
  ApartmentCard(this.apartment);
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        minimum: EdgeInsets.only(top: 8.0),
        child: GestureDetector(
            onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        ApartmentPage(apartment))),
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      apartment.photo == null
                          ? Container()
                          : CarouselSlider(items: createImagesList()),
                      Padding(
                          padding: EdgeInsets.only(
                              top: 8.0,
                              left: APARTMENT_CARD_LEFT_TEXT_PADDING,
                              bottom: 4.0),
                          child: Text(apartment.price + ' ₽',
                              style: Theme.of(context).textTheme.title)),
                      Padding(
                        padding: EdgeInsets.only(
                            top: 8,
                            left: APARTMENT_CARD_LEFT_TEXT_PADDING,
                            bottom: 8.0),
                        child: Text('Квартира ' +
                            (apartment.roomCount == 0
                                ? 'студия ${apartment.area}м²'
                                : apartment.roomCount.toString() +
                                    '-комн., ${apartment.area}м²')),
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                              left: APARTMENT_CARD_LEFT_TEXT_PADDING,
                              bottom: 8.0,
                              top: 4.0),
                          child: Text(apartment.fullAddress,
                              style: TextStyle(
                                  color: Colors.grey[600], fontSize: 14))),
                      Padding(
                        padding: EdgeInsets.only(
                            top: 4.0, left: SIDE_PADDING, right: SIDE_PADDING),
                        child: Container(
                            height: PICKER_HEIGHT,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4.0),
                                border:
                                    Border.all(color: Colors.grey, width: 1)),
                            child: Center(child: Text('Позвонить'))),
                      )
                    ],
                  ),
                ))));
  }

  List<Widget> createImagesList() {
    List<Widget> imagesList = List();

    apartment.photo.forEach((photo) => imagesList.add(PictureItem(photo)));
    return imagesList;
  }
}

class PictureItem extends StatelessWidget {
  final String photoUrl;

  PictureItem(this.photoUrl);

  Widget build(BuildContext context) {
    return Container(
        child: Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(4.0),
              child: CachedNetworkImage(
                width: MediaQuery.of(context).size.width,
                imageUrl: photoUrl,
                fit: BoxFit.cover,
                placeholder: (_, __) => Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        width: double.infinity,
                        height: double.infinity,
                        decoration: BoxDecoration(
                            color: Colors.grey[100],
                            borderRadius: BorderRadius.circular(4.0)),
                      ),
                    ),
              ),
            )));
  }
}
