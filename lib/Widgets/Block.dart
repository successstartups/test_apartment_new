import 'package:flutter/material.dart';
import 'package:test_apartment/Other/Colors.dart';
import 'package:test_apartment/Other/Offsets.dart';

class Block extends StatelessWidget {
  final Widget child;

  Block({this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: BLOCK_BOTTOM_MARGIN),
      color: BLOCK_BACKGROUND_COLOR, child: child);
  }
}
