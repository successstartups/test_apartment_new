import 'package:flutter/material.dart';
import 'package:test_apartment/Other/Offsets.dart';
import 'package:test_apartment/Views/DistrictList.dart';

class DistrictPicker extends StatefulWidget {
  @override
  _DistrictPickerState createState() => _DistrictPickerState();
}

class _DistrictPickerState extends State<DistrictPicker> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => DistrictList())),
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Padding(
              padding: EdgeInsets.only(
                  left: ROOM_COUNT_BUTTON_SIDE_PADDING, top: 4.0, bottom: 4.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Район'),
                  Text('Не выбрано', style: TextStyle(color: Colors.grey))
                ],
              )),
        ));
  }
}
