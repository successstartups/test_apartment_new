import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_apartment/Blocs/RoomCountBloc/Filter.dart';

import 'package:test_apartment/Other/Offsets.dart';

class PropertyTypePicker extends StatefulWidget {
  @override
  _PropertyTypePickerState createState() => _PropertyTypePickerState();
}

class _PropertyTypePickerState extends State<PropertyTypePicker> {
  String propertyType = 'Квартиру';
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => showPropertyTypeList(
            context: context,
            child: CupertinoActionSheet(
              actions: <Widget>[
                CupertinoActionSheetAction(
                  child: Text('Квартиру'),
                  onPressed: () => {
                        Navigator.pop(context, 'Квартиру'),
                        BlocProvider.of<FilterBloc>(context)
                            .dispatch(ApartmentSelected())
                      },
                ),
                CupertinoActionSheetAction(
                  child: Text('Комнату'),
                  onPressed: () => {
                        Navigator.pop(context, 'Комнату'),
                        BlocProvider.of<FilterBloc>(context)
                            .dispatch(RoomSelected())
                      },
                ),
                CupertinoActionSheetAction(
                  child: Text('Дом'),
                  onPressed: () => {
                        Navigator.pop(context, 'Дом'),
                        BlocProvider.of<FilterBloc>(context)
                            .dispatch(HouseSelected())
                      },
                ),
                CupertinoActionSheetAction(
                  child: Text('Коммерческую'),
                  onPressed: () => {
                        Navigator.pop(context, 'Коммерческую'),
                        BlocProvider.of<FilterBloc>(context)
                            .dispatch(CommercialSelected())
                      },
                ),
              ],
            )),
        child: Container(
            width: (MediaQuery.of(context).size.width / 2) -
                (SIDE_PADDING + SMALL_PADDING),
            margin: EdgeInsets.only(right: SIDE_PADDING, left: SMALL_PADDING),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              color: Colors.white,
            ),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Padding(
                        padding: EdgeInsets.only(left: MEDIUM_PADDING),
                        child: Text(
                          propertyType.toString(),
                          style: Theme.of(context).textTheme.body2,
                        ))))));
  }

  void showPropertyTypeList({BuildContext context, Widget child}) {
    showCupertinoModalPopup<String>(
      context: context,
      builder: (BuildContext context) => child,
    ).then((String value) {
      if (value != null) {
        setState(() {
          propertyType = value;
        });
      }
    });
  }
}
