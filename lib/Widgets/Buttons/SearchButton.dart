import 'package:flutter/material.dart';

class SearchButton extends StatefulWidget {
  final String title;
  final Function onTap;
  final Color color;

  SearchButton({this.title, this.onTap, this.color});

  @override
  _SearchButtonState createState() => _SearchButtonState();
}

class _SearchButtonState extends State<SearchButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        height: 50,
        width: MediaQuery.of(context).size.width,
        color: widget.color,
        child: Center(
          child: Text(
            widget.title,
            style: Theme.of(context).textTheme.body2.copyWith(color: Colors.white),
          ),
        ),
      ),
    );
  }
}
