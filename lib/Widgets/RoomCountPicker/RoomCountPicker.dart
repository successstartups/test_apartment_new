import 'package:flutter/material.dart';
import 'package:test_apartment/Model/FilterRequest.dart';
import 'package:test_apartment/Other/Colors.dart';
import 'package:test_apartment/Other/Fonts.dart';
import 'package:test_apartment/Other/Offsets.dart';

import 'RoomCountButton.dart';

class RoomCountPicker extends StatefulWidget {
  Function onTap;
  int count;

  RoomCountPicker({@required this.onTap, @required this.count});

  @override
  _RoomCountPickerState createState() => _RoomCountPickerState();
}

class _RoomCountPickerState extends State<RoomCountPicker> {
  List roomsCount = List<int>();

  List<bool> selectList;

  @override
  void initState() {
    selectList = [false, false, false, false, false];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    //подгоняем епта ы
    double x = screenWidth -
        (SIDE_PADDING * 2) -
        (4 * SMALL_PADDING) -
        (5 * screenWidth * (1 / 6));

    var buttonsList = [
      RoomCountButton(
        title: 'Студия',
        color: selectList[0] ? BLUE : Colors.white,
        active: selectList[0] ? PICKER_WHITE_TEXT : PICKER_BLACK_TEXT,
        onTap: () => numberSelected(0),
        width: screenWidth * (1 / 6) + x,
        index: 0,
        totalCount: 5,
      ),
      RoomCountButton(
        title: '1',
        color: selectList[1] ? BLUE : Colors.white,
        active: selectList[1] ? PICKER_WHITE_TEXT : PICKER_BLACK_TEXT,
        onTap: () => numberSelected(1),
        width: screenWidth * (1 / 6),
        index: 1,
        totalCount: 5,
      ),
      RoomCountButton(
        title: '2',
        color: selectList[2] ? BLUE: Colors.white,
        active: selectList[2] ? PICKER_WHITE_TEXT : PICKER_BLACK_TEXT,
        onTap: () => numberSelected(2),
        width: screenWidth * (1 / 6),
        index: 2,
        totalCount: 5,
      ),
      RoomCountButton(
        title: '3',
        color: selectList[3] ? BLUE : Colors.white,
        active: selectList[3] ? PICKER_WHITE_TEXT : PICKER_BLACK_TEXT,
        onTap: () => numberSelected(3),
        width: screenWidth * (1 / 6),
        index: 3,
        totalCount: 5,
      ),
      RoomCountButton(
        title: '4+',
        color: selectList[4] ? BLUE : Colors.white,
        active: selectList[4] ? PICKER_WHITE_TEXT : PICKER_BLACK_TEXT,
        onTap: () => numberSelected(4),
        width: screenWidth * (1 / 6),
        index: 4,
        totalCount: 5,
      ),
    ];

    return Padding(
        padding: EdgeInsets.only(
            bottom: SIDE_PADDING, left: SIDE_PADDING, right: SIDE_PADDING),
        child: Container(
            child: Row(
          children: buttonsList,
        )));
  }

  void numberSelected(int count) {
    selectList[count] = !selectList[count];
    widget.onTap();
    if (roomsCount.contains(count)) {
      setState(() {
        roomsCount.remove(count);
      });
    } else {
      setState(() {
        roomsCount.add(count);
      });

      roomsCount.sort();

      FilterRequest().rooms = roomsCount;
    }
  }
}
