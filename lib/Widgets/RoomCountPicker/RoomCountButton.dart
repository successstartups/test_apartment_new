import 'package:flutter/material.dart';
import 'package:test_apartment/Other/Offsets.dart';

class RoomCountButton extends StatefulWidget {
  final double width;
  final String title;
  Function onTap;
  Color color;
  final int index;
  final int totalCount;
  TextStyle active;

  RoomCountButton(
      {@required this.title,
      @required this.width,
      @required this.index,
      @required this.totalCount,
      this.onTap,
      @required this.color, this.active});

  @override
  _RoomCountButtonState createState() => _RoomCountButtonState();
}

class _RoomCountButtonState extends State<RoomCountButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: widget.onTap,
        child: Container(
          margin: widget.index != widget.totalCount - 1
              ? EdgeInsets.only(right: SMALL_PADDING)
              : null,
          decoration: BoxDecoration(
              color: widget.color,
              borderRadius: BorderRadius.all(Radius.circular(4.0))),
          height: 40,
          width: widget.width,
          child: Center(child: Text(widget.title, style: widget.active)),
        ));
  }
}
