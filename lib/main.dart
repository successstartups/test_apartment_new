import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_apartment/Api/ApiProvider.dart';

import 'Blocs/RoomCountBloc/Filter.dart';
import 'Views/ApartmentsListPage.dart';
import 'Views/FilterPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // ApartmentsFilterBloc _apartmentsFilterBloc;
  // RoomCountBloc _roomCountBloc;
  @override
  void initState() {
    // _apartmentsFilterBloc = ;
    // _apartmentsFilterBloc.dispatch(SecondaryPropertySelected());

    //_roomCountBloc = RoomCountBloc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProviderTree(
        blocProviders: [
          BlocProvider<FilterBloc>(
              builder: (BuildContext context) => FilterBloc())
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          initialRoute: '/',
          routes: {
            '/': (context) => FilterPage(),
            '/list': (context) => ApartmentsListPage(),
          },
          theme: ThemeData(
            primarySwatch: Colors.blue,
            fontFamily: 'Montserrat',
            textTheme: TextTheme(
                headline:
                    TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
                title: TextStyle(
                  fontSize: 20.0,
                ),
                body1: TextStyle(fontSize: 14.0),
                body2:
                    TextStyle(fontSize: 16.0, fontWeight: FontWeight.normal)),
          ),
        ));
  }
}
