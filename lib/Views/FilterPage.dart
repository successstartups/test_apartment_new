import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_apartment/Blocs/RoomCountBloc/Filter.dart';
import 'package:test_apartment/Model/FilterRequest.dart';

import 'package:test_apartment/Other/Colors.dart';
import 'package:test_apartment/Other/Offsets.dart';
import 'package:test_apartment/Widgets/Buttons/SearchButton.dart';
import 'package:test_apartment/Widgets/CustomBar.dart';
import 'package:test_apartment/Widgets/DealTypePicker/DealTypePicker.dart';
import 'package:test_apartment/Widgets/PriceField/PriceField.dart';
import 'package:test_apartment/Widgets/PropertyTypePicker/PropertyTypePicker.dart';
import 'package:test_apartment/Widgets/RentTypePicker/RentTypePicker.dart';
import 'package:test_apartment/Widgets/RoomCountPicker/RoomCountPicker.dart';

import 'package:test_apartment/Widgets/TimeBuildingPicker/TimeBuildingPicker.dart';

enum DEALTYPE { BUY, RENT }

class FilterPage extends StatefulWidget {
  @override
  _FilterPageState createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  @override
  void initState() {
    BlocProvider.of<FilterBloc>(context).dispatch(BuyTypeSelected());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomBar(),
        backgroundColor: BACKGROUND,
        body: Stack(
          children: <Widget>[
            BlocBuilder(
                bloc: BlocProvider.of<FilterBloc>(context),
                builder: (BuildContext context, FilterBlocState state) {
                  var filterFields =
                      (state as RoomFilterPageState).filterFields;
                  if (filterFields['buildingType'] == null) {
                    FilterRequest().buildingStatus = 'Все';
                  }
                  return ListView(
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.only(bottom: SIDE_PADDING),
                          height: PICKER_HEIGHT,
                          child: Row(children: <Widget>[
                            DealTypePicker(),
                            PropertyTypePicker()
                          ])),
                     // if (filterFields['rentType'] != null) RentTypePicker(),
                      if (filterFields['buildingType'] != null)
                        TimeBuildingPicker(),
                      if (filterFields['roomCount'])
                        RoomCountPicker(
                          onTap: () => print('+'),
                          count: 5,
                        ),
                      PriceField()
                    ],
                  );
                }),
            Positioned(
              bottom: 0,
              child: SearchButton(
                title: 'Показать предложения',
                onTap: () => Navigator.pushNamed(context, '/list'),
                color: BLUE,
              ),
            )
          ],
        ));
  }
}
