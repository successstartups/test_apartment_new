import 'dart:async';

import 'package:flutter/material.dart';
import 'package:test_apartment/Model/Apartment.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:test_apartment/Other/Colors.dart';
import 'package:test_apartment/Other/Offsets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:test_apartment/Widgets/ApartmentCard.dart';
import 'package:test_apartment/Widgets/Block.dart';

class ApartmentPage extends StatefulWidget {
  @override
  _ApartmentPageState createState() => _ApartmentPageState();

  ApartmentPage(this.apartment);
  final Apartment apartment;
}

class _ApartmentPageState extends State<ApartmentPage> {
  List<Widget> images = List<Widget>();
  Completer<GoogleMapController> _controller = Completer();

  @override
  void initState() {
    widget.apartment.photo.forEach((f) => images.add(PictureItem(f)));
    super.initState();
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  bool _checkHouseInfo() {
    return (widget.apartment.bathroom != '' ||
            widget.apartment.foundation != '')
        ? true
        : false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: BACKGROUND,
        body: ListView(children: <Widget>[
          Block(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _images(),
              Padding(
                  padding: EdgeInsets.only(
                      left: APARTMENT_CARD_LEFT_TEXT_PADDING,
                      bottom: 8.0,
                      top: 4.0),
                  child: Text(widget.apartment.price + ' ₽',
                      style: Theme.of(context).textTheme.title)),
              Padding(
                padding: EdgeInsets.only(
                    left: APARTMENT_CARD_LEFT_TEXT_PADDING, bottom: 8.0),
                child: Text('Квартира' +
                    (widget.apartment.roomCount == 0
                        ? ' студия ${widget.apartment.area}м²'
                        : widget.apartment.roomCount.toString() +
                            '-комн., ${widget.apartment.area}м²')),
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: APARTMENT_CARD_LEFT_TEXT_PADDING, bottom: 8.0),
                child: Text('${widget.apartment.priceMeter.toString()} ₽ за м²',
                    style: TextStyle(color: Colors.grey[600], fontSize: 14)),
              ),
            ],
          )),
          Block(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(
                        left: APARTMENT_CARD_LEFT_TEXT_PADDING,
                        bottom: 12.0,
                        top: 12.0),
                    child: Text('${widget.apartment.fullAddress}',
                        style: Theme.of(context).textTheme.body1)),
                Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(bottom: 4.0),
                    height: 200,
                    child: GoogleMap(
                      initialCameraPosition: CameraPosition(
                        target: LatLng(widget.apartment.latitude,
                            widget.apartment.longitude),
                        zoom: 12.0,
                      ),
                      onMapCreated: _onMapCreated,
                      markers: {
                        Marker(
                            markerId: MarkerId('1'),
                            position: LatLng(widget.apartment.latitude,
                                widget.apartment.longitude))
                      },
                    ))
              ],
            ),
          ),
          Block(
            child: _specifications(),
          ),
          _checkHouseInfo() ? _interBlocksHeader('ДОМ') : Container(),
          Block(
            child: _houseInfo(),
          ),
          widget.apartment.description != ''
              ? Block(
                  child: _descriptionText(),
                )
              : Container(),
          widget.apartment.agent != null
              ? Block(
                  child: Center(child: _agentInfo()),
                )
              : Container(),
        ]));
  }

  Widget _agentInfo() {
    String phone = widget.apartment.agent['phone'];
    return Padding(
      padding: EdgeInsets.only(top: 8.0),
      child: Column(
        children: <Widget>[
          // Container(
          //   decoration:
          //       BoxDecoration(color: Colors.white, shape: BoxShape.circle),
          //   // height: 50.0,
          //   // width: 50.0,
          //   child: Icon(
          //     Icons.face,
          //     size: 25.0,
          //     color: Colors.grey,
          //   ),
          // ),

          Padding(
            padding: EdgeInsets.only(top: 8.0),
            child: Text('Агенство недвижимости Метро',

                style: Theme.of(context).textTheme.body1),
          ),
          Padding(
            padding: EdgeInsets.only(top: 8.0),
            child: Text(
              widget.apartment.agent['name'],
              style: Theme.of(context).textTheme.body1,
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: _contactButton()),
        ],
      ),
    );
  }

  Widget _contactButton(){
    return Container(

    );
  }

  Widget _descriptionText() {
    return Padding(
      padding: EdgeInsets.only(top: 15.0, left: SIDE_PADDING, bottom: 8.0),
      child: Text(
        widget.apartment.description,
        style: Theme.of(context).textTheme.body1,
      ),
    );
  }

  Widget _interBlocksHeader(String text) {
    return Padding(
      padding: EdgeInsets.only(top: 15.0, left: SIDE_PADDING, bottom: 8.0),
      child: Text(
        text,
        style: TextStyle(color: Colors.grey[600], fontSize: 12),
      ),
    );
  }

  Widget _houseInfo() {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: SIDE_PADDING),
        child: Column(children: <Widget>[
          Padding(padding: EdgeInsets.only(bottom: 4.0)),
          widget.apartment.foundation != ''
              ? _specificationString(
                  'Год постройки', widget.apartment.foundation.toString())
              : Container(),
          widget.apartment.bathroom != ''
              ? _specificationString(
                  'Санузел', widget.apartment.bathroom.toString())
              : Container(),
        ]));
  }

  Widget _specifications() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: SIDE_PADDING),
      child: Column(
        children: <Widget>[
          Padding(padding: EdgeInsets.only(bottom: 4.0)),
          _specificationString(
              'Этаж',
              widget.apartment.floor.toString() +
                  ' из ' +
                  widget.apartment.floors.toString()),
          _specificationString(
              'Количество комнат', widget.apartment.roomCount.toString()),
          _specificationString(
              'Общая площадь', widget.apartment.sumArea.toString() + ' м²'),
          _specificationString(
              '  Жилая площадь', widget.apartment.area.toString() + ' м²'),
          _specificationString('  Площадь кухни',
              widget.apartment.kitchenArea.toString() + ' м²'),
        ],
      ),
    );
  }

  Widget _specificationString(String left, String right) {
    return Container(
      margin: EdgeInsets.only(top: 4.0, bottom: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(left, style: Theme.of(context).textTheme.body1),
          Text(right, style: Theme.of(context).textTheme.body1)
        ],
      ),
    );
  }

  Widget _images() {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 4.0),
        child: CarouselSlider(
          initialPage: 1,
          enableInfiniteScroll: false,
          scrollDirection: Axis.horizontal,
          items: images,
        ));
  }
}
