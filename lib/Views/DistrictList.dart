import 'package:flutter/material.dart';
import 'package:test_apartment/Model/District.dart';

class DistrictList extends StatefulWidget {
  @override
  _DistrictListState createState() => _DistrictListState();
}

class _DistrictListState extends State<DistrictList> {
  List districtList = List<District>();

  initState() {
    districtList.add(District('Ярославль', DistrictType.CITY, false));
    districtList.add(District('Семибратово', DistrictType.VILLAGE, false));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Выбрать район'),
      ),
      body: ListView.builder(
          itemCount: districtList.length,
          itemBuilder: (BuildContext context, int i) {
            return listTile(districtList[i]);
          }),
    );
  }

  Widget listTile(District district) {
    return CheckboxListTile(
      value: district.selected,
      title: Text(district.name),
      onChanged: (bool newValue) => setState(() {
            district.selected = newValue;
          }),
    );
  }
}
