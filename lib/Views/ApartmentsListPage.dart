import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:test_apartment/Blocs/ListBloc/List.dart';
import 'package:test_apartment/Model/Apartment.dart';
import 'package:test_apartment/Widgets/ApartmentCard.dart';

class ApartmentsListPage extends StatefulWidget {
  ApartmentsListPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ApartmentsListPageState createState() => _ApartmentsListPageState();
}

class _ApartmentsListPageState extends State<ApartmentsListPage> {
  ListBloc _listBloc;
  List<Apartment> apartments = List<Apartment>();
  ScrollController _controler = ScrollController();
  final _scrollThreshold = 200.0;

  void _onScroll() {
    final maxScroll = _controler.position.maxScrollExtent;
    final currentScroll = _controler.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _listBloc.dispatch(Fetch());
    }
  }

  @override
  void initState() {
    _listBloc = ListBloc();
    _listBloc.dispatch(Fetch());
    _controler.addListener(_onScroll);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder(
        bloc: _listBloc,
        builder: (BuildContext context, ListState state) {
          if (state is Loading) {
            return Center(child:  SpinKitThreeBounce(
                      size: 22, color: Colors.lightBlue.shade400));
          }

          if (state is LoadingComplete) {
            return ListView.builder(
              controller: _controler,
              itemCount: state.list.length,
              itemBuilder: (BuildContext context, int index) {
                return ApartmentCard(state.list[index]);
              },
            );
          }
        },
      ),
    );
  }
}
