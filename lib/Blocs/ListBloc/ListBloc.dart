import 'package:bloc/bloc.dart';
import 'package:rxdart/rxdart.dart';

import 'package:test_apartment/Api/ApiProvider.dart';
import 'package:test_apartment/Model/Apartment.dart';

import 'List.dart';

class ListBloc extends Bloc<ListEvent, ListState> {
  @override
  Stream<ListState> transform(
    Stream<ListEvent> events,
    Stream<ListState> Function(ListEvent event) next,
  ) {
    return super.transform(
      (events as Observable<ListEvent>).debounceTime(
        Duration(milliseconds: 500),
      ),
      next,
    );
  }

  @override
  get initialState => Loading();
  int offset = 0;

  @override
  Stream<ListState> mapEventToState(ListEvent event) async* {
    if (event is Fetch) {
      print('-');
      yield LoadingComplete((currentState is Loading
              ? List<Apartment>()
              : (currentState as LoadingComplete).list) +
          await ApiProvider().getApartmentsList(offset));
      offset += 10;
    }
  }
}
