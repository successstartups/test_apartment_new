import 'package:equatable/equatable.dart';
import 'package:test_apartment/Model/Apartment.dart';

class ListState extends Equatable {}

class Loading extends ListState {}

class LoadingComplete extends ListState
    with EquatableMixinBase, EquatableMixin {
  final List<Apartment> list;

  LoadingComplete(this.list);

  List get props => super.props..addAll([list]);
}
