import 'package:bloc/bloc.dart';
import 'package:test_apartment/Model/FilterRequest.dart';

import 'Filter.dart';

enum DealType { BUY, RENT }
enum RentType { LONG, DAILY }
enum BuildingType { ALL, NEW, SECOND }
enum EstateType { APARTMENT, ROOM, HOUSE, AREA, COMERCIAL }

// List<String> estateType = ['Квартиру', 'Комнату', 'Дом', 'Участок', 'Комерческую'];

class FilterBloc extends Bloc<FilterBlocEvent, FilterBlocState> {
  @override
  FilterBlocState get initialState => RoomFilterPageState(filter);

  Map<String, dynamic> filter = {
    'dealType': DealType.BUY,
    'estateType': EstateType.APARTMENT,
    'rentType': null,
    'buildingType': BuildingType.ALL,
    'roomCount': true,
    'price': true
  };

  @override
  Stream<FilterBlocState> mapEventToState(FilterBlocEvent event) async* {
    //выбран сброс фильтра
    if (event is ResetFilter) {
      filter['estateType'] = EstateType.APARTMENT;
      if (filter['dealType'] == DealType.BUY) {
        filter['roomCount'] = true;
        filter['buildingType'] = BuildingType.ALL;
        filter['rentType'] = null;
        filter['roomCount'] = true;
      }

      yield RoomFilterPageState(filter);
    }

    //выбрана комната
    if (event is RoomSelected) {
      FilterRequest().type = 'room';
      filter['estateType'] = EstateType.ROOM;
      if (filter['dealType'] == DealType.BUY) {
        filter['roomCount'] = false;
        filter['buildingType'] = BuildingType.ALL;
      } else {
        filter['rentType'] = RentType.LONG;
        filter['roomCount'] = false;
        filter['buildingType'] = null;
      }

      yield RoomFilterPageState(filter);
    }

    //выбрана квартира
    if (event is ApartmentSelected) {
      FilterRequest().type = 'flat';
      filter['estateType'] = EstateType.APARTMENT;

      if (filter['dealType'] == DealType.BUY) {
        filter['roomCount'] = true;
        filter['buildingType'] = BuildingType.ALL;
      } else {
        filter['rentType'] = RentType.LONG;
        filter['roomCount'] = true;
        filter['buildingType'] = null;
      }
      yield RoomFilterPageState(filter);
    }

    //выбран дом

    if (event is HouseSelected) {
      FilterRequest().type = 'house';
      filter['estateType'] = EstateType.HOUSE;

      if (filter['dealType'] == DealType.BUY) {
        filter['roomCount'] = false;
        filter['buildingType'] = null;
      } else {
        filter['rentType'] = RentType.LONG;
        filter['roomCount'] = false;
        filter['buildingType'] = null;
      }
      yield RoomFilterPageState(filter);
    }

    //выбрана коммерческая

    if (event is CommercialSelected) {
      FilterRequest().type = 'commercial';
      filter['estateType'] = EstateType.COMERCIAL;

      if (filter['dealType'] == DealType.BUY) {
        filter['roomCount'] = false;
        filter['buildingType'] = null;
      } else {
        filter['rentType'] = RentType.LONG;
        filter['roomCount'] = false;
        filter['buildingType'] = null;
      }
      yield RoomFilterPageState(filter);
    }

    //снять
    if (event is RentTypeSelected) {
      FilterRequest().dealType = 'rent';
      filter['dealType'] = DealType.RENT;
      filter['buildingType'] = null;
      filter['rentType'] = RentType.LONG;

      yield RoomFilterPageState(filter);
    }

    //купить
    if (event is BuyTypeSelected) {
      FilterRequest().dealType = 'buy';
      filter['dealType'] = DealType.BUY;
      filter['buildingType'] = BuildingType.ALL;
      filter['estateType'] = EstateType.APARTMENT;
      filter['rentType'] = null;

      yield RoomFilterPageState(filter);
    }
  }
}
