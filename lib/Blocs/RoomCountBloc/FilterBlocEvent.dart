import 'package:equatable/equatable.dart';

class FilterBlocEvent extends Equatable {}

//Правая
class RoomSelected extends FilterBlocEvent {}

class ApartmentSelected extends FilterBlocEvent {}

class HouseSelected extends FilterBlocEvent {}

class CommercialSelected extends FilterBlocEvent {}

class AreaSelected extends FilterBlocEvent {}

//Левая
class RentTypeSelected extends FilterBlocEvent {}

class BuyTypeSelected extends FilterBlocEvent {}

//сбросить фильтр
class ResetFilter extends FilterBlocEvent {}
