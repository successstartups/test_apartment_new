import 'package:equatable/equatable.dart';

class FilterBlocState extends Equatable {}

class RoomCountVisible extends FilterBlocState {}

class RoomCountHide extends FilterBlocState {}

class RentTypeVisible extends FilterBlocState {}

class RentTypeHide extends FilterBlocState {}

class RoomFilterPageState extends FilterBlocState with EquatableMixinBase, EquatableMixin 
{
  final Map<String, dynamic> filterFields;
  RoomFilterPageState(filterFields) : this.filterFields = Map.from(filterFields);

  List get props => super.props..addAll([filterFields]);
}