class Apartment {
  //адрес
  String city;
  //улица
  String street;
  //цена за метр
  String priceMeter;
  //стоимость
  String price;
  //количество комнат
  int roomCount;
  //этаж
  int floor;
  //этажей всего
  int floors;
  //жилая площадь
  double area;
  //общая площадь
  double sumArea;
  //площадь кухни
  double kitchenArea;
  //год постройки
  String foundation;
  //полный адрес
  String fullAddress;
  //данные агента
  Map agent;
  //фотографии
  List photo;
  //метка для геолокации
  double latitude;
  double longitude;
  //описание
  String description;
  //тип туалета
  String bathroom;

  Apartment(
      this.city,
      this.street,
      this.roomCount,
      this.area,
      this.sumArea,
      this.kitchenArea,
      this.photo,
      this.price,
      this.priceMeter,
      this.description,
      this.fullAddress,
      this.latitude,
      this.longitude,
      this.floor,
      this.floors,
      this.bathroom,
      this.foundation,
      this.agent,);

  factory Apartment.fromJson(Map<String, dynamic> json) {
    return Apartment(
        json['address']['city'],
        json['address']['street'],
        json['rooms'],
        json['area'].toDouble(),
        json['sumArea'].toDouble(),
        json['kitchenArea'].toDouble(),
        json['image'],
        json['price'],
        json['priceMeter'],
        json['description'],
        json['address']['full'],
        json['geo']['latitude'],
        json['geo']['longitude'],
        json['floor'],
        json['floors'],
        json['bathroom'],
        json['foundation'],
        json['agent'],);
  }
}
