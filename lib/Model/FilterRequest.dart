class FilterRequest {
  static final FilterRequest instance = FilterRequest._private();

  FilterRequest._private();
  factory FilterRequest() => instance;

  String minPrice = '0';

  String maxPrice = '';

  List<int> rooms = [];

  String buildingStatus = 'all';

  String type = 'flat';

  String dealType = 'buy';
}
