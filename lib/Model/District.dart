class District {
  final String name;
  DistrictType type;
  bool selected;

  District(this.name, this.type, this.selected);
}

enum DistrictType { CITY, VILLAGE }
