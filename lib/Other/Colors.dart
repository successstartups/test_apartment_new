import 'package:flutter/material.dart';

const Color BLOCK_BACKGROUND_COLOR = Colors.white;
const Color BACKGROUND = Color.fromARGB(255, 237, 238, 240);
const LIGTH_GRAY = Color.fromARGB(255, 237, 238, 240);

const BLUE = Color.fromARGB(255, 9, 97, 160);

const RED = Color.fromARGB(255, 205, 38, 40);
