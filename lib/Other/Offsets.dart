const double ROOM_COUNT_BUTTON_SIDE_PADDING = 12.0;
const double BLOCK_BOTTOM_MARGIN = 8.0;
const double APARTMENT_CARD_LEFT_TEXT_PADDING = 15.0;
const double SIDE_PADDING = 15.0;
const double MEDIUM_PADDING = 16.0;
const double PICKER_HEIGHT = 40;
const double SMALL_PADDING = 5.0;
